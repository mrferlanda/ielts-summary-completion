from newspaper import Article
import re

def scrape(article_url):
    article = Article(article_url)
    article.download()
    article.html
    article.parse()
    title = article.title
    content = article.text.replace("\n"," ")
    content = re.sub(' +', ' ', content)
    return title, content