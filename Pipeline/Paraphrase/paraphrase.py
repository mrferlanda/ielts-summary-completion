from transformers import PegasusForConditionalGeneration, PegasusTokenizerFast
import nltk

model = PegasusForConditionalGeneration.from_pretrained("tuner007/pegasus_paraphrase")
tokenizer = PegasusTokenizerFast.from_pretrained("tuner007/pegasus_paraphrase")

tokenizer_sen = nltk.data.load('tokenizers/punkt/english.pickle')


def get_paraphrased_sentences(model, tokenizer, sentence, num_return_sequences=1, num_beams=5):
      # tokenize the text to be form of a list of token IDs
  inputs = tokenizer([sentence], truncation=True, padding="longest", return_tensors="pt" )
  # generate the paraphrased sentences
  outputs = model.generate(
    **inputs,
    num_beams=num_beams,
    num_return_sequences=num_return_sequences,
    max_length=1024
  )
  # decode the generated sentences using the tokenizer to get them back to text
  return tokenizer.batch_decode(outputs, skip_special_tokens=True)

def paraphrase(text):
    text_list = tokenizer_sen.tokenize(text)
    res = []
    for sentence in text_list:
        paraphrased_sentences = get_paraphrased_sentences(model, tokenizer, sentence, num_return_sequences=5, num_beams=5)
        res.append(paraphrased_sentences[0])
    paraphrased_text = " ".join(res)
    return paraphrased_text