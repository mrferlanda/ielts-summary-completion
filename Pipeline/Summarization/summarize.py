from azure.ai.textanalytics import TextAnalyticsClient, ExtractSummaryAction
from azure.core.credentials import AzureKeyCredential

key = "5d81924ff58748e7be0cff5cbe8cbd00"
endpoint = "https://ferlanda-cognitive-service.cognitiveservices.azure.com/"

def authenticate_client():
    ta_credential = AzureKeyCredential(key)
    text_analytics_client = TextAnalyticsClient(
            endpoint=endpoint, 
            credential=ta_credential)
    return text_analytics_client

def sample_extractive_summarization(client, original_text):

    document = [
    ]
    document.append(original_text)
    poller = client.begin_analyze_actions(
        document,
        actions=[
            ExtractSummaryAction(MaxSentenceCount=8)
        ],
    )

    document_results = poller.result()
    for result in document_results:
        extract_summary_result = result[0]  # first document, first result
        if extract_summary_result.is_error:
            print("...Is an error with code '{}' and message '{}'".format(
                extract_summary_result.code, extract_summary_result.message
            ))
        else:
            text_result =" ".join([sentence.text for sentence in extract_summary_result.sentences])
            
    return text_result

def summarize(original_text):
        client = authenticate_client()
        return sample_extractive_summarization(client, original_text)