from cmath import inf
from random import random
import pandas as pd 
import nltk

from sentence_transformers import SentenceTransformer, util
import numpy as np
from sklearn.utils import shuffle
from nltk.corpus import wordnet as wn
from nltk.stem.porter import *
import sys
import os

from nltk.stem import WordNetLemmatizer
from hunspell import Hunspell
from functools import lru_cache
root_dir = os.getcwd()
nle_dir = os.path.join(root_dir, 'Pipeline', 'Utility', 'nodebox_linguistics_extended')
with open (r"log.txt", "w") as f:
    f.write(nle_dir)
sys.path.insert(0, nle_dir)
import nodebox_linguistics_extended as nle
model = SentenceTransformer('all-mpnet-base-v2')

def lev_dist(a, b):
    '''
    This function will calculate the levenshtein distance between two input
    strings a and b
    
    params:
        a (String) : The first string you want to compare
        b (String) : The second string you want to compare
        
    returns:
        This function will return the distnace between string a and b.
        
    example:
        a = 'stamp'
        b = 'stomp'
        lev_dist(a,b)
        >> 1.0
    '''
    
    @lru_cache(None)  # for memorization
    def min_dist(s1, s2):

        if s1 == len(a) or s2 == len(b):
            return len(a) - s1 + len(b) - s2

        # no change required
        if a[s1] == b[s2]:
            return min_dist(s1 + 1, s2 + 1)

        return 1 + min(
            min_dist(s1, s2 + 1),      # insert character
            min_dist(s1 + 1, s2),      # delete character
            min_dist(s1 + 1, s2 + 1),  # replace character
        )

    return min_dist(0, 0)

def frequency_calculation(text):
    killgariff_db  = os.path.join(root_dir, 'Pipeline', 'Utility', 'all.al')
    df = pd.read_csv(killgariff_db , header=None , sep=" ")
    df.columns = ['total_freq' , 'word' , 'pos_tag' , 'others']
    df.drop(['others'], axis=1, inplace=True)
    df = df[~df['total_freq'].isnull()]
    # df = df[df['total_freq'].str.isnumeric()]
    # df = df.astype({"total_freq": int}, errors='raise') 

    sentences_from_text = nltk.sent_tokenize(text)
    pos_tag_text = []
    for sentence in sentences_from_text:
        pos_tag_text.append(nltk.pos_tag(nltk.word_tokenize(sentence)))
    words_dict = []
    for i,sentence in enumerate(pos_tag_text):
        for word,tag in sentence:
            res = df[df['word'] == word].groupby('word').sum()['total_freq']
            if (not res.empty):
                words_dict.append({
                    'word' : word,
                    'tag' : tag,
                    'freq' : res.values[0],
                    'sentence' : i
                })
    df_words = pd.DataFrame(words_dict)
    df_words.sort_values(by='freq', ascending=False, inplace=True)

    allowed_tags = ['JJ', 'JJR', 'JJS', 'NN', 'NNS', 'NNP', 'NNPS', 'RB', 'RBR', 'RBS', 'VB', 'VBD', 'VBG', 'VBN', 'VBP', 'VBZ']
    df_words = df_words.drop_duplicates(subset='word', keep='first')
    df_words = df_words[df_words["tag"].isin(allowed_tags)]
    
    word_candidate_list = []

    for i in range(len(pos_tag_text)):
        idx = int(df_words[df_words["sentence"] == i].shape[0] /2)
        add_word_candidate_by_idx(df_words,word_candidate_list, idx-1, i)
        add_word_candidate_by_idx(df_words,word_candidate_list, idx, i)
        add_word_candidate_by_idx(df_words,word_candidate_list, idx+1, i)

    df_result = pd.DataFrame(word_candidate_list)
    # df_result.to_pickle("word_candidate_list.pkl")
    return df_result
    

def add_word_candidate_by_idx(df_words,word_candidate_list, idx, sentence_idx):
    word_candidate_list.append({
        "word" : df_words[df_words["sentence"] == sentence_idx].iloc[idx]["word"],
        "tag" : df_words[df_words["sentence"] == sentence_idx].iloc[idx]["tag"],
        "sentence_no" : sentence_idx
    })

def pos_convert(pos):
    if pos.startswith('J'):
        return wn.ADJ
    elif pos.startswith('V'):
        return wn.VERB
    elif pos.startswith('N'):
        return wn.NOUN
    elif pos.startswith('R'):
        return wn.ADV

def word_similarity(model,word1, word2):
    """
    Returns the similarity between two words.
    """
    # encode sentences to get their embeddings
    embedding1 = model.encode(word1, convert_to_tensor=True)
    embedding2 = model.encode(word2, convert_to_tensor=True)
    # compute similarity scores of two embeddings
    cosine_scores = util.pytorch_cos_sim(embedding1, embedding2)
    return cosine_scores.item()

def get_synonym_distractor_row(sen,row):
    idx_sen = row['sentence_no']
    passage_sen = sen[idx_sen]
    word = row["word"]
    tag = row["tag"]
    if len(word) < 2:
        return row
    try:
        word = nle.verb.present(word)
    except:
        pass
    lemmatizer = WordNetLemmatizer()
    lemmatized_word = lemmatizer.lemmatize(word, pos=pos_convert(tag))
    best_distractor = "<no distractor>"
    best_synonym = "<no synonym>"
    
    best_synonym_score = -1 # -1 is the lowest possible score
    for syn in wn.synsets(word):
        ex_size = len(syn.examples()) 
        if(ex_size > 0):
            sum_score = 0
            for i in range(ex_size):
                score = word_similarity(model,passage_sen,syn.examples()[i])
                sum_score += score
            row['sentence_sim_score'] = sum_score / ex_size
            for l in syn.lemmas():
                lemmatized_syn_word = lemmatizer.lemmatize(l.name())
                if ((lemmatized_word != lemmatized_syn_word ) and (lemmatized_word not in lemmatized_syn_word ) 
                     and (lemmatized_syn_word not in lemmatized_word)):
                    score = word_similarity(model,lemmatized_word,lemmatized_syn_word)
                    if score > best_synonym_score:
                        best_synonym = lemmatized_syn_word.replace('_', ' ')
                        best_synonym = best_synonym.lower()
                        best_synonym_score = score
                        if l.antonyms():
                            best_distractor = l.antonyms()[0].name().replace('_', ' ')
                            df = pd.DataFrame({
                                "distractor": [best_distractor], 
                                "transformed_distractor": [best_distractor], 
                                "tag" : [row['tag']]})
                            df = df.apply(lambda x: transform_word(x,"distractor", "transformed_distractor"), axis=1)
                            best_distractor = df["transformed_distractor"].values[0]
            row["synonym"] = best_synonym
            row["distractor"] = best_distractor
            print(best_synonym, best_distractor)
            row["synonym_score"] = best_synonym_score

    return row
def get_homophone_distractor_row(row):
    if(row['distractor'] == "<no distractor>"):
        hunspell = Hunspell()
        print(row['distractor'], row['transformed_synonym']  )
        homophones = hunspell.suggest(row['transformed_synonym'])
        print(homophones, len(homophones))
        if(len(homophones) > 0):
            min_dist = inf
            for homophone in homophones:
                if(" " not in homophone and homophone.lower() != row['transformed_synonym']):
                    dist = lev_dist(row['transformed_synonym'], homophone)
                    if(dist < min_dist):
                        min_dist = dist
                        row['distractor'] = homophone
    return row

def get_synonym_distractor(sen,df_word_candidate):

    df_word_candidate = pd.DataFrame(df_word_candidate)

    df_word_candidate["synonym"] = np.nan
    df_word_candidate["sentence_sim_score"] = np.nan
    df_word_candidate["synonym_score"] = np.nan
    df_word_candidate["distractor"] = np.nan
 

    df_word_candidate = df_word_candidate.apply(lambda x: get_synonym_distractor_row(sen,x), axis=1)
    print(df_word_candidate.to_string())
    idx = df_word_candidate.groupby(["sentence_no"])['sentence_sim_score'].transform(max) == df_word_candidate['sentence_sim_score']
    df_word_candidate = df_word_candidate[idx]

    df_word_candidate = df_word_candidate[df_word_candidate['synonym'] != "<no synonym>"]
    

    df_word_candidate['transformed_synonym'] = np.nan
    df_word_candidate = df_word_candidate.apply(lambda x: transform_word(x,"synonym", "transformed_synonym") , axis=1)

    df_word_candidate = df_word_candidate.apply(lambda x: get_homophone_distractor_row(x), axis=1)    
    
    return df_word_candidate


def transform_word(row,target_name,col_name):
    syn = row[target_name]
    syn_split = syn.split(' ')
    first_word = syn_split[0]
    try:
        if(row['tag'] == 'VBD'): #past tense
            transformed_word = nle.verb.past(first_word)
        elif(row['tag'] == 'VBG'): #present participle
            transformed_word = nle.verb.present_participle(first_word)
        elif(row['tag'] == 'VBN'): #past particile 
            transformed_word = nle.verb.past_participle(first_word)
        elif(row['tag'] == 'VBZ'):
            transformed_word = nle.noun.plural(first_word)
        elif(row['tag'] == 'NNS'):
            transformed_word = nle.noun.plural(first_word)
        elif(row['tag'] == 'NNPS'):
            transformed_word = nle.noun.plural(first_word)
        elif (row['word'][-3:] == 'ing'):
            transformed_word = nle.verb.present_participle(first_word)
        else:
            transformed_word = first_word
        syn_split[0] = transformed_word
        row[col_name] = (" ").join(syn_split)

    except:
        row[col_name] = syn
        print("error in transforming word")

    return row

def post_process(sentences_from_text,df_word_candidate):
    sentences_from_text = nltk.sent_tokenize(sentences_from_text)
    random_choices = []
    correct_answer = []
    distractor = []
    
    print(df_word_candidate, file=sys.stderr)
    # print(df_synonyms.to_string())
    # print(df_distractors.to_string())

    for _, row in df_word_candidate.iterrows():
        idx_sen = row['sentence_no']
        correct_answer.append(row['transformed_synonym'])
        random_choices.append(row['transformed_synonym'])
        if row['distractor'] != "<no distractor>":
            distractor.append(row['distractor'])
            random_choices.append(row['distractor'])
        sentences_from_text[idx_sen] = sentences_from_text[idx_sen].replace(row['word'], "____" , 1)
    random_choices = shuffle(random_choices)
    final_text = " ".join(sentences_from_text)
    return final_text,correct_answer,distractor, random_choices