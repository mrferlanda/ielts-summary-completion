import argparse
from Pipeline.ArticleScrape.scrape import scrape
from Pipeline.Summarization.summarize import summarize
from Pipeline.Utility.util import frequency_calculation, get_synonym_distractor
from Pipeline.Utility.util import post_process
import sys
import datetime
import nltk
import os
sys.path.insert(0, r"D:\Skripsi\IPYNB\Pipeline\Utility\nodebox_linguistics_extended")

def parse_opt():
    parser = argparse.ArgumentParser()
    parser.add_argument('-a', '--article', type=str, required=True, help='Article URL')
    parser.add_argument('-o', '--output', type=str, required=True, help='Output Directory')
    return parser.parse_args()

def main(opt):
    begin = datetime.datetime.now()
    title, content = scrape(opt.article)
    full_text = title + "\n\n" + content
    res_dir = os.path.join('Result',opt.output)
    os.makedirs(res_dir, exist_ok=True)
    with open(os.path.join(res_dir,"articles.txt") ,'w+') as f:
        f.write(full_text)
    summary_text = summarize(content)
    summary_token = nltk.sent_tokenize(summary_text)
    if ( len (summary_token) < 3):
        return "Text too short, find another article"
    with open(os.path.join(res_dir,"summary.txt"), 'w+') as f:
        f.write(summary_text)
    
    # paraphrased_text = paraphrase(summary_text)
    # with open('Result/paraphrase.txt', 'w+') as f:
    #     f.write(paraphrased_text)
    
    word_candidate_df = frequency_calculation(summary_text)
    df_word_candidate  = get_synonym_distractor(summary_text, word_candidate_df)
    final_text, correct_answers, distractors, random_choices = post_process(summary_text, df_word_candidate)
    with open(os.path.join(res_dir,"correct_answers.txt"), 'w+') as f:
        f.write(str(correct_answers))
    with open(os.path.join(res_dir,"distractors.txt"), 'w+') as f:
        f.write(str(distractors))
    with open(os.path.join(res_dir,"final_text.txt"), 'w+') as f:
        f.write(final_text)
    with open(os.path.join(res_dir,"random_choices.txt"), 'w+') as f:
        f.write(str(random_choices))
    print("Total Time: ", datetime.datetime.now() - begin)
    return full_text, final_text, random_choices, correct_answers

if __name__ == '__main__':
    opt = parse_opt()
    main(opt)