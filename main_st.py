import ast
import datetime
import sys
from turtle import title
import streamlit as st
import os 
import nltk
with st.spinner('Installing NLTK Packages...'):
    nltk.download('wordnet')
    nltk.download('punkt')
    nltk.download('averaged_perceptron_tagger')

st.title('IELTS Summary Completion Generator')
st.write('This is a web application that can be used to generate summary completion question for IELTS test')

with st.form("my_form"):
    st.write("Inside the form")
    folder_name = st.text_input('Folder Name')
    url = st.text_input('Article URL')

    # Every form must have a submit button.
    submitted = st.form_submit_button("Submit")
    if submitted:
        try:
            with st.spinner(text='Wait for it...'):
                begin = datetime.datetime.now()
                os.system('python main.py -a ' + url + ' -o ' + folder_name)

                direction_txt = """
                Complete the summary using the list of words below.
                Write the correct letter in boxes on your answer sheet.
                """


                res_dir = os.path.join('Result',folder_name)
                with open(os.path.join(res_dir,"articles.txt") ,'r') as f:
                    title = f.readline()
                    content = f.read()
                with open (os.path.join(res_dir, 'final_text.txt'), 'r') as f:
                    final_text = f.read()
                with open (os.path.join(res_dir, 'random_choices.txt'), 'r') as f:
                    random_choices = ast.literal_eval(f.read())
                with open (os.path.join(res_dir, 'correct_answers.txt'), 'r') as f:
                    correct_answers = ast.literal_eval(f.read())
                st.title(title)
                st.markdown('<div style="text-align: justify;">    ' + content + '</div>' + "\n", unsafe_allow_html=True)
                st.write("\n")
                st.markdown('_' + direction_txt + '_', unsafe_allow_html=True)
                st.write("\n")
                st.write(final_text, "\n")

                for i in range (len(random_choices)):
                    st.write(chr(65+i) + ". " + random_choices[i], " ")
                correct_answers_txt = ', '.join(correct_answers)
                st.write("\n");
                st.write("Correct answers (Ordered): " + correct_answers_txt, "\n")
        except:
            st.error("Error occured. Please try another input.")
        finally:
            st.success('Done!')
            st.write('Total Time: ', datetime.datetime.now() - begin)
           
